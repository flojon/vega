using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Bson;
using MongoDB.Driver;
using vega.api.Models;
using vega.api.Resources;

namespace vega.api.Controllers
{
  [Route("api/[controller]")]
  [ApiController]
  public class FeaturesController : ControllerBase
  {
    private readonly IMongoCollection<Feature> collection;
    private readonly IMapper mapper;

    public FeaturesController(IMongoDatabase database, IMapper mapper)
    {
      this.collection = database.GetCollection<Feature>("features");
      this.mapper = mapper;
    }

    [HttpGet]
    public async Task<ActionResult<IEnumerable<FeatureResource>>> Get()
    {
      var features = await this.collection.Find(new BsonDocument()).ToListAsync();

      var result = mapper.Map<List<Feature>, IEnumerable<Feature>>(features);

      return Ok(result);
    }

    [HttpGet("{id}")]
    public async Task<ActionResult<FeatureResource>> Get(string id)
    {
      var feature = await collection.Find(x => x.Id == id).SingleOrDefaultAsync();
      if (feature == null)
        return NotFound("No document with the given id was found");

      var result = mapper.Map<Feature, FeatureResource>(feature);

      return Ok(result);
    }

    [HttpPost]
    public async Task<ActionResult<FeatureResource>> Create(SaveFeatureResource featureResource)
    {
      var feature = mapper.Map<SaveFeatureResource, Feature>(featureResource);
      await collection.InsertOneAsync(feature);

      var result = mapper.Map<Feature, FeatureResource>(feature);

      return Ok(feature);
    }

    [HttpPut("{id}")]
    public async Task<ActionResult<FeatureResource>> Update(string id, SaveFeatureResource featureResource)
    {
      var feature = mapper.Map<SaveFeatureResource, Feature>(featureResource);
      feature.Id = id;
      var options = new FindOneAndReplaceOptions<Feature>
      {
        ReturnDocument = ReturnDocument.After
      };
      var updatedFeature = await collection.FindOneAndReplaceAsync<Feature>(x => x.Id == id, feature, options);

      if (updatedFeature == null)
        return NotFound("No document with the given id was found");

      var result = mapper.Map<Feature, FeatureResource>(updatedFeature);

      return Ok(result);
    }

    [HttpDelete("{id}")]
    public async Task<ActionResult<FeatureResource>> Delete(string id)
    {
      var feature = await collection.FindOneAndDeleteAsync<Feature>(x => x.Id == id);

      if (feature == null)
        return NotFound("No document with the given id was found");

      var result = mapper.Map<Feature, FeatureResource>(feature);

      return Ok(result);
    }
  }
}
