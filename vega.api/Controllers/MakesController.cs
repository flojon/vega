using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Bson;
using MongoDB.Driver;
using vega.api.Models;
using vega.api.Resources;

namespace vega.api.Controllers
{
  [Route("api/[controller]")]
  [ApiController]
  public class MakesController : ControllerBase
  {
    private readonly IMongoCollection<Make> collection;
    private readonly IMapper mapper;

    public MakesController(IMongoDatabase database, IMapper mapper)
    {
      this.collection = database.GetCollection<Make>("makes");
      this.mapper = mapper;
    }

    [HttpGet]
    public async Task<ActionResult<IEnumerable<MakeResource>>> Get()
    {
      var makes = await this.collection.Find(new BsonDocument()).ToListAsync();
      var makeResources = mapper.Map<List<Make>, IEnumerable<MakeResource>>(makes);

      return Ok(makeResources);
    }

    [HttpGet("{id}")]
    public async Task<ActionResult<MakeResource>> Get(string id)
    {
      var make = await collection.Find(x => x.Id == id).SingleOrDefaultAsync();
      if (make == null)
        return NotFound("No document with the given id was found");

      var makeResource = mapper.Map<Make, MakeResource>(make);

      return Ok(makeResource);
    }

    [HttpPost]
    public async Task<ActionResult<MakeResource>> Create(SaveMakeResource makeResouce)
    {
      var make = mapper.Map<SaveMakeResource, Make>(makeResouce);
      await collection.InsertOneAsync(make);

      var result = mapper.Map<Make, MakeResource>(make);

      return Ok(result);
    }

    [HttpPut("{id}")]
    public async Task<ActionResult<MakeResource>> Update(string id, SaveMakeResource makeResource)
    {
      var make = mapper.Map<SaveMakeResource, Make>(makeResource);
      make.Id = id;
      var options = new FindOneAndReplaceOptions<Make>
      {
        ReturnDocument = ReturnDocument.After
      };
      var makeUpdated = await collection.FindOneAndReplaceAsync<Make>(x => x.Id == id, make, options);

      if (makeUpdated == null)
        return NotFound("No document with the given id was found");

      var result = mapper.Map<Make, MakeResource>(makeUpdated);

      return Ok(result);
    }

    [HttpDelete("{id}")]
    public async Task<ActionResult<MakeResource>> Delete(string id)
    {
      var make = await collection.FindOneAndDeleteAsync<Make>(x => x.Id == id);

      if (make == null)
        return NotFound("No document with the given id was found");

      var makeResouce = mapper.Map<Make, MakeResource>(make);

      return Ok(makeResouce);
    }
  }
}
