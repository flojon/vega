using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Bson;
using MongoDB.Driver;
using vega.api.Models;
using vega.api.Resources;

namespace vega.api.Controllers
{
  [Route("api/[controller]")]
  [ApiController]
  public class VehiclesController : ControllerBase
  {
    private readonly IMongoCollection<Vehicle> vehicles;
    private readonly IMongoCollection<Feature> features;
    private readonly IMongoCollection<Make> makes;
    private readonly IMapper mapper;

    public VehiclesController(IMongoDatabase database, IMapper mapper)
    {
      this.vehicles = database.GetCollection<Vehicle>("vehicles");
      this.features = database.GetCollection<Feature>("features");
      this.makes = database.GetCollection<Make>("makes");
      this.mapper = mapper;
    }

    [HttpGet]
    public async Task<ActionResult<IEnumerable<VehicleResource>>> Get()
    {
      var vehicles = await this.vehicles.Find(new BsonDocument()).ToListAsync();
      var vehicleResources = mapper.Map<List<Vehicle>, IEnumerable<VehicleResource>>(vehicles);

      return Ok(vehicleResources);
    }

    [HttpGet("{id}")]
    public async Task<ActionResult<VehicleResource>> Get(string id)
    {
      var vehicle = await vehicles.Find(x => x.Id == id).SingleOrDefaultAsync();
      if (vehicle == null)
        return NotFound("No document with the given id was found");

      var vehicleResource = mapper.Map<Vehicle, VehicleResource>(vehicle);

      return Ok(vehicleResource);
    }

    [HttpPost]
    public async Task<ActionResult<VehicleResource>> Create(SaveVehicleResource vehicleResource)
    {
      var make = await makes.Find(m => m.Id == vehicleResource.MakeId).SingleOrDefaultAsync();
      if (make == null)
        return BadRequest("Invalid Make ID");

      var features = await this.features.Find(f => vehicleResource.FeatureIds.Contains(f.Id)).ToListAsync();
      if (features == null || features.Count != vehicleResource.FeatureIds.Count)
        return BadRequest("One or more invalid feature ids given");

      var vehicle = mapper.Map<SaveVehicleResource, Vehicle>(vehicleResource);
      vehicle.Make = mapper.Map<Make, IdName>(make);
      vehicle.Features = mapper.Map<List<Feature>, ICollection<IdName>>(features);

      await vehicles.InsertOneAsync(vehicle);

      var result = mapper.Map<Vehicle, VehicleResource>(vehicle);

      return Ok(result);
    }

    [HttpPut("{id}")]
    public async Task<ActionResult<VehicleResource>> Update(string id, SaveVehicleResource vehicleResource)
    {
      var make = await makes.Find(m => m.Id == vehicleResource.MakeId).SingleOrDefaultAsync();
      if (make == null)
        return BadRequest("Invalid Make ID");

      var features = await this.features.Find(f => vehicleResource.FeatureIds.Contains(f.Id)).ToListAsync();
      if (features == null || features.Count != vehicleResource.FeatureIds.Count)
        return BadRequest("One or more invalid feature ids given");

      var vehicle = mapper.Map<SaveVehicleResource, Vehicle>(vehicleResource);
      vehicle.Id = id;
      vehicle.Make = mapper.Map<Make, IdName>(make);
      vehicle.Features = mapper.Map<List<Feature>, ICollection<IdName>>(features);

      var options = new FindOneAndReplaceOptions<Vehicle>
      {
        ReturnDocument = ReturnDocument.After
      };
      var vehicleUpdated = await vehicles.FindOneAndReplaceAsync<Vehicle>(x => x.Id == id, vehicle, options);

      if (vehicleUpdated == null)
        return NotFound("No document with the given id was found");

      var result = mapper.Map<Vehicle, VehicleResource>(vehicleUpdated);

      return Ok(result);
    }

    [HttpDelete("{id}")]
    public async Task<ActionResult<VehicleResource>> Delete(string id)
    {
      var vehicle = await vehicles.FindOneAndDeleteAsync<Vehicle>(x => x.Id == id);

      if (vehicle == null)
        return NotFound("No document with the given id was found");

      var result = mapper.Map<Vehicle, VehicleResource>(vehicle);

      return Ok(result);
    }
  }
}
