using AutoMapper;
using vega.api.Models;
using vega.api.Resources;

namespace vega.api.MappingProfiles
{
  public class ContactMappingProfile : Profile
  {
    public ContactMappingProfile()
    {
      CreateMap<Contact, ContactResource>()
        .ReverseMap();
    }
  }
}
