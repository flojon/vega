using AutoMapper;
using vega.api.Models;
using vega.api.Resources;

namespace vega.api.MappingProfiles
{
  public class FeatureMappingProfile : Profile
  {
    public FeatureMappingProfile()
    {
      CreateMap<Feature, FeatureResource>();
      CreateMap<SaveFeatureResource, Feature>();
    }
  }
}
