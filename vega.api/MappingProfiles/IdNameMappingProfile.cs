using AutoMapper;
using vega.api.Models;
using vega.api.Resources;

namespace vega.api.MappingProfiles
{
  public class IdNameMappingProfile : Profile
  {
    public IdNameMappingProfile()
    {
      CreateMap<IdName, IdNameResource>()
        .ReverseMap();
      CreateMap<Make, IdName>();
      CreateMap<Feature, IdName>();
    }
  }
}
