using AutoMapper;
using vega.api.Models;
using vega.api.Resources;

namespace vega.api.MappingProfiles
{
  public class MakeMappingProfile : Profile
  {
    public MakeMappingProfile()
    {
      CreateMap<Make, MakeResource>();
      CreateMap<SaveMakeResource, Make>();
    }
  }
}
