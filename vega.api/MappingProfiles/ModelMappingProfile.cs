using AutoMapper;
using vega.api.Models;
using vega.api.Resources;

namespace vega.api.MappingProfiles
{
  public class ModelMappingProfile : Profile
  {
    public ModelMappingProfile()
    {
      CreateMap<Model, ModelResource>();
    }
  }
}