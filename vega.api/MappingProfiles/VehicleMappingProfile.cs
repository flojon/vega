using AutoMapper;
using vega.api.Models;
using vega.api.Resources;

namespace vega.api.MappingProfiles
{
  public class VehicleMappingProfile : Profile
  {
    public VehicleMappingProfile()
    {
      CreateMap<Vehicle, VehicleResource>();
      CreateMap<SaveVehicleResource, Vehicle>();
    }
  }
}