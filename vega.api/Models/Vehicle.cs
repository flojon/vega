using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson.Serialization.IdGenerators;

namespace vega.api.Models
{
  public class Vehicle
  {
    [BsonId(IdGenerator = typeof(StringObjectIdGenerator))]
    [BsonRepresentation(BsonType.ObjectId)]
    public string Id { get; set; }
    public IdName Make { get; set; }
    public string Model { get; set; }
    public bool Registered { get; set; }
    public ICollection<IdName> Features { get; set; }
    public Contact Contact { get; set; }
  }
}
