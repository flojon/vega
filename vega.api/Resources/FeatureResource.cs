namespace vega.api.Resources
{
  public class FeatureResource
  {
    public string Id { get; set; }
    public string Name { get; set; }
  }
}