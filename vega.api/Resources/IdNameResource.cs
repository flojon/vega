namespace vega.api.Resources
{
  public class IdNameResource
  {
    public string Id { get; set; }
    public string Name { get; set; }
  }
}