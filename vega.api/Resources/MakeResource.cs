using System.Collections.Generic;

namespace vega.api.Resources
{
  public class MakeResource
  {
    public string Id { get; set; }
    public string Name { get; set; }
    public ICollection<ModelResource> Models { get; set; }
  }
}