using System.Collections.Generic;

namespace vega.api.Resources
{
  public class SaveMakeResource
  {
    public string Name { get; set; }
    public ICollection<ModelResource> Models { get; set; }
  }
}