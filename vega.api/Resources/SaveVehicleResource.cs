using System.Collections.Generic;

namespace vega.api.Resources
{
  public class SaveVehicleResource
  {
    public string Id { get; set; }
    public string MakeId { get; set; }
    public string Model { get; set; }
    public bool Registered { get; set; }
    public ICollection<string> FeatureIds { get; set; }
    public ContactResource Contact { get; set; }
  }
}
