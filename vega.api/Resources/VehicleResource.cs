using System.Collections.Generic;

namespace vega.api.Resources
{

  public class VehicleResource
  {
    public string Id { get; set; }
    public IdNameResource Make { get; set; }
    public string Model { get; set; }
    public bool Registered { get; set; }
    public ICollection<IdNameResource> Features { get; set; }
    public ContactResource Contact { get; set; }
  }
}
