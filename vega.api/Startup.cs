﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using MongoDB.Bson.Serialization.Conventions;
using MongoDB.Driver;

namespace vega.api
{
  public class Startup
  {
    public Startup(IConfiguration configuration)
    {
      Configuration = configuration;

      var pack = new ConventionPack();
      pack.Add(new CamelCaseElementNameConvention());

      ConventionRegistry.Register(
         "Camel Case Conventions",
         pack,
         t => true);
    }

    public IConfiguration Configuration { get; }

    // This method gets called by the runtime. Use this method to add services to the container.
    public void ConfigureServices(IServiceCollection services)
    {
      services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
      services.AddAutoMapper();
      services.AddSingleton<MongoClient>(provider =>
      {
        return new MongoClient(Configuration.GetValue("MongoDB:URL", ""));
      });
      services.AddTransient<IMongoDatabase>(provider =>
      {
        var client = provider.GetService<MongoClient>();
        return client.GetDatabase(Configuration.GetValue("MongoDB:Database", "default"));
      });
    }

    // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
    public void Configure(IApplicationBuilder app, IHostingEnvironment env)
    {
      if (env.IsDevelopment())
      {
        app.UseDeveloperExceptionPage();
        app.UseCors(builder =>
        {
          builder.WithOrigins(new[] { "https://localhost:4200" });
          builder.AllowAnyMethod();
          builder.AllowAnyHeader();
        });
      }
      else
      {
        app.UseHsts();
      }

      app.UseHttpsRedirection();
      app.UseMvc();
    }
  }
}
