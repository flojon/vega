import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { ReactiveFormsModule } from "@angular/forms";
import { HttpClientModule } from "@angular/common/http";

import { NgbModule } from "@ng-bootstrap/ng-bootstrap";

import { AppComponent } from "./app.component";
import { VehicleFormComponent } from "./vehicle-form/vehicle-form.component";
import { VegaService } from "./vega-service/vega.service";
import { VehicleListComponent } from "./vehicle-list/vehicle-list.component";
import { VehicleFormModalComponent } from "./vehicle-form-modal/vehicle-form-modal.component";

@NgModule({
  declarations: [
    AppComponent,
    VehicleFormComponent,
    VehicleListComponent,
    VehicleFormModalComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    ReactiveFormsModule,
    NgbModule.forRoot()
  ],
  providers: [VegaService],
  bootstrap: [AppComponent],
  entryComponents: [VehicleFormModalComponent]
})
export class AppModule {}
