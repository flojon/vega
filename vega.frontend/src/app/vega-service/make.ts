import { Model } from "./model";
export class Make {
  id: string;
  name: string;
  models: Model[];
}
