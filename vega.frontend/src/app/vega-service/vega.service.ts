import { Injectable } from "@angular/core";
import { environment } from "../../environments/environment";
import { HttpClient } from "@angular/common/http";

import { Observable } from "rxjs";

import { Feature } from "./feature";
import { Make } from "./make";
import { SaveVehicle, Vehicle } from "./vehicle";

@Injectable({
  providedIn: "root"
})
export class VegaService {
  constructor(private http: HttpClient) {}

  public getMakes(): Observable<Make[]> {
    return this.http.get<Make[]>(environment.vegaApiBaseUrl + "makes");
  }

  public getFeatures(): Observable<Feature[]> {
    return this.http.get<Feature[]>(environment.vegaApiBaseUrl + "features");
  }

  public getVehicles(): Observable<Vehicle[]> {
    return this.http.get<Vehicle[]>(environment.vegaApiBaseUrl + "vehicles");
  }

  public createVehicle(vehicle: SaveVehicle): Observable<Vehicle> {
    return this.http.post<Vehicle>(
      environment.vegaApiBaseUrl + "vehicles",
      vehicle
    );
  }
}
