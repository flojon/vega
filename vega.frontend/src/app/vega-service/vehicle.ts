import { Contact } from "./contact";

export class SaveVehicle {
  makeId: string;
  model: string;
  registered: boolean;
  featureIds: string[];
  contact: Contact;
}

export class IdName {
  id: string;
  name: string;
}

export class Vehicle {
  id: string;
  make: IdName;
  model: string;
  registered: boolean;
  features: IdName[];
  contact: Contact;
}
