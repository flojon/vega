import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VehicleFormModalComponent } from './vehicle-form-modal.component';

describe('VehicleFormModalComponent', () => {
  let component: VehicleFormModalComponent;
  let fixture: ComponentFixture<VehicleFormModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VehicleFormModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VehicleFormModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
