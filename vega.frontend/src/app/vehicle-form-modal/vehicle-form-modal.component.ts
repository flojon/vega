import { Component, OnInit } from "@angular/core";
import { NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";
import { VehicleFormComponent } from "../vehicle-form/vehicle-form.component";
import { Vehicle } from "../vega-service/vehicle";

@Component({
  selector: "app-vehicle-form-modal",
  templateUrl: "./vehicle-form-modal.component.html",
  styleUrls: ["./vehicle-form-modal.component.css"]
})
export class VehicleFormModalComponent implements OnInit {
  constructor(private activeModal: NgbActiveModal) {}

  ngOnInit() {}

  onAfterSave(vehicle: Vehicle) {
    this.activeModal.close(vehicle);
  }
}
