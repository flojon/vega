import { Component, OnInit, EventEmitter, Output } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";

import { VegaService } from "../vega-service/vega.service";
import { Make } from "../vega-service/make";
import { Feature } from "../vega-service/feature";
import { Vehicle } from "../vega-service/vehicle";

@Component({
  selector: "vehicle-form",
  templateUrl: "./vehicle-form.component.html",
  styleUrls: ["./vehicle-form.component.css"]
})
export class VehicleFormComponent implements OnInit {
  @Output() afterSave = new EventEmitter<Vehicle>();
  vehicleForm: FormGroup;
  makes: Make[];
  features: Feature[];

  constructor(private vegaService: VegaService, private fb: FormBuilder) {}

  ngOnInit() {
    this.vegaService.getMakes().subscribe(makes => {
      this.makes = makes;
    });
    this.vegaService.getFeatures().subscribe(features => {
      this.features = features;
      this.vehicleForm = this.fb.group({
        makeId: this.fb.control("", Validators.required),
        model: this.fb.control("", Validators.required),
        registered: this.fb.control("", Validators.required),
        featureIds: this.fb.array(
          this.features.map(f => this.fb.control(false))
        ),
        contact: this.fb.group({
          name: this.fb.control("", Validators.required),
          phone: this.fb.control("", Validators.required),
          email: this.fb.control("")
        })
      });
    });
  }

  get models() {
    const makeId = this.vehicleForm.get("makeId").value;
    if (!makeId) return [];

    var make = this.makes.find(m => m.id == makeId);
    if (!make) return [];

    return make.models;
  }

  get featureIds() {
    return this.vehicleForm.get("featureIds");
  }

  save() {
    const vehicle = Object.assign({}, this.vehicleForm.value, {
      featureIds: this.features
        .filter(
          (value, index, array) =>
            this.vehicleForm.get("featureIds").value[index]
        )
        .map(f => f.id)
    });

    this.vegaService
      .createVehicle(vehicle)
      .subscribe(v => this.afterSave.emit(v));
  }
}
