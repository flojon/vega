import { Component, OnInit } from "@angular/core";
import { Observable } from "rxjs";

import { VegaService } from "../vega-service/vega.service";
import { Vehicle } from "../vega-service/vehicle";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { VehicleFormModalComponent } from "../vehicle-form-modal/vehicle-form-modal.component";

@Component({
  selector: "vehicle-list",
  templateUrl: "./vehicle-list.component.html",
  styleUrls: ["./vehicle-list.component.css"]
})
export class VehicleListComponent implements OnInit {
  vehicles: Observable<Vehicle[]>;
  constructor(
    private vegaService: VegaService,
    private modalService: NgbModal
  ) {}

  ngOnInit() {
    this.vehicles = this.vegaService.getVehicles();
  }

  onAddVehicleClick() {
    const modalRef = this.modalService.open(VehicleFormModalComponent);
    modalRef.result.then(
      v => {
        this.vehicles = this.vegaService.getVehicles();
      },
      x => console.log("closed", x)
    );
  }
}
